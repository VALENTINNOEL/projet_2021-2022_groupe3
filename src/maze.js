import * as THREE from 'three';
import {OBJLoader} from 'three/examples/jsm/loaders/OBJLoader.js';
import {MTLLoader} from 'three/examples/jsm/loaders/MTLLoader.js';
import * as TWEEN from "@tweenjs/tween.js";

/**
 * Maze class
 * @param wrapper
 * @param button
 */
class ThreeMaze {
    constructor(window, wrapper, width, height) {
        // Object attributes
        this.window = window
        this.wrapper = wrapper;
        this.camera = {};
        this.scene = {};
        this.materials = {};
        this.map = [];
        this.renderer = {};
        this.end = {};
        this.width = width;
        this.height = height;
        this.thickness = 30;
        this.players = [];
    }

    generateEmptyMap = (width, height) => {
        var map = [];

        for (var x = 1; x <= width; x += 1) {
            map[x] = [];
            for (var y = 1; y <= height; y += 1) {
                map[x][y] = 0;
            }
        }

        return map;
    }

    onGenerateMaze(map_representation) {
        var new_map = this.generateEmptyMap(this.width, this.height);

        //check if hider is captured
        map_representation['captured'].forEach(player => {
            this.players[player].player.visible = false;
        })

        //check if seeker finds hider
        if (map_representation["observationSeeker"] !== undefined) {
            this.players[map_representation["observationSeeker"]["name"]].player.children[1].visible = map_representation["observationSeeker"]["visible"];
        }

        for (var x = this.width; x > 0; x -= 1) {
            for (var y = 1; y < this.height + 1; y += 1) {
                // store current kind of cell
                new_map[x][y] = {
                    'kind': map_representation[x][y] // block or player
                }

                // check if necessary to update (need to be improved => some blocks can be damaged)
                if (typeof this.map[x] != 'undefined' && typeof this.map[x][y] != 'undefined' && new_map[x][y].kind === this.map[x][y].kind) {

                    new_map[x][y] = this.map[x][y];
                    continue;

                } else if (typeof this.map[x] != 'undefined' && typeof this.map[x][y] != 'undefined') {

                    // check if player and take care in this case of the two mesh
                    if (this.map[x][y].player !== undefined) {

                        //this.map[x][y].player.visible = false;
                        this.map[x][y].mesh.visible = false;
                        //this.scene.remove(this.map[x][y].player);
                        this.scene.remove(this.map[x][y].mesh);
                    } else {
                        this.map[x][y].mesh.visible = false;
                        this.scene.remove(this.map[x][y].mesh);
                    }
                }

                // Adds a new mesh if needed
                if (map_representation[x][y]["id"] === "BORDER") {
                    // Generates the mesh for border
                    var wall_geometry = new THREE.BoxGeometry(this.thickness, this.thickness * 2, this.thickness, 1, 1, 1);
                    new_map[x][y].mesh = new THREE.Mesh(wall_geometry, this.materials.grey);
                    new_map[x][y].mesh.visible = true;
                    new_map[x][y].mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                    this.scene.add(new_map[x][y].mesh);

                } else if (map_representation[x][y]["id"] === "GROUND") {
                    // generate the mesh with ground
                    var ground = new THREE.BoxGeometry(this.thickness, this.thickness / 4, this.thickness, 1, 1, 1);
                    new_map[x][y].mesh = new THREE.Mesh(ground, this.materials.ground);
                    new_map[x][y].mesh.visible = true;
                    new_map[x][y].mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                    this.scene.add(new_map[x][y].mesh);
                } else if (map_representation[x][y]["id"] === "HIDER") {
                    // Add hider
                    let rotation;
                    if (this.players[map_representation[x][y]["name"]] === undefined) {
                        // init hider
                        const mtlLoader = new MTLLoader();
                        let map = new_map[x][y];
                        let map_representation_x_y = map_representation[x][y];
                        let position = [];
                        position["x"] = x * this.thickness - ((this.width * this.thickness) / 2);
                        position["y"] = this.thickness / 2;
                        position["z"] = y * this.thickness - ((this.height * this.thickness) / 2);
                        // load hider
                        new Promise((resolve, reject) => {
                            mtlLoader.load('data/modeles/Road_Runner/road_runner.mtl', (materials) => {
                                materials.preload();
                                const objLoader = new OBJLoader();
                                objLoader.setMaterials(materials);
                                objLoader.load('data/modeles/Road_Runner/road_runner.obj', (objet) => {
                                    const boite = new THREE.Box3();
                                    boite.setFromObject(objet);
                                    let scale = this.thickness * 2 / boite.max.y;
                                    objet.scale.x = scale;
                                    objet.scale.y = scale;
                                    objet.scale.z = scale;
                                    resolve(objet)
                                })
                            })
                        })
                            .then(obj => {
                                    // add block on hider
                                    var wall_geometry = new THREE.BoxGeometry(0.25, 0.25, 0.25, 1, 1, 1);
                                    var block = new THREE.Mesh(wall_geometry, this.materials.stone);
                                    block.visible = map_representation_x_y["block"]["visible"];
                                    block.name = "block";
                                    obj.add(block);
                                    block.position.y = 0.65;

                                    map.player = obj;
                                    map.player.position.set(position["x"], position["y"], position["z"]);
                                    // add orientation
                                    if (map_representation_x_y["orientation"] === "Top") {
                                        map.player.rotateY(-Math.PI / 2);
                                        rotation = Math.PI / 2;

                                    } else if (map_representation_x_y["orientation"] === "Bottom") {
                                        map.player.rotateY(Math.PI / 2);
                                        rotation = -Math.PI / 2;

                                    } else if (map_representation_x_y["orientation"] === "Left") {
                                        map.player.rotateY(Math.PI);
                                        rotation = -Math.PI;
                                    } else {
                                        map.player.rotateY(0);
                                        rotation = 0;

                                    }
                                    this.players[map_representation_x_y["name"]] = {
                                        player: map.player,
                                        rotation: rotation,
                                    };
                                    this.scene.add(map.player);
                                }
                            )
                    } else {
                        // change position
                        new_map[x][y].player = this.players[map_representation[x][y]["name"]].player;
                        new_map[x][y].player.visible = true;
                        var tween = new TWEEN.Tween(new_map[x][y].player.position).to({
                            x: x * this.thickness - ((this.width * this.thickness) / 2),
                            y: this.thickness / 2,
                            z: y * this.thickness - ((this.height * this.thickness) / 2)
                        }, 1000);
                        tween.start();

                        // add block on hider
                        new_map[x][y].player.children[4].visible = map_representation[x][y]["block"]["visible"];
                        if (map_representation[x][y]["block"]["materialStr"] === "WOOD") {
                            new_map[x][y].player.children[4].material = this.materials.wood;
                        } else {
                            new_map[x][y].player.children[4].material = this.materials.stone;
                        }

                        //add orientation
                        new_map[x][y].player.rotateY(this.players[map_representation[x][y]["name"]].rotation);
                        if (map_representation[x][y]["orientation"] === "Top") {
                            new_map[x][y].player.rotateY(-Math.PI / 2);
                            rotation = Math.PI / 2;

                        } else if (map_representation[x][y]["orientation"] === "Bottom") {
                            new_map[x][y].player.rotateY(Math.PI / 2);
                            rotation = -Math.PI / 2;

                        } else if (map_representation[x][y]["orientation"] === "Left") {
                            new_map[x][y].player.rotateY(Math.PI);
                            rotation = -Math.PI;
                        } else {
                            new_map[x][y].player.rotateY(0);
                            rotation = 0;

                        }
                        this.players[map_representation[x][y]["name"]].player = new_map[x][y].player;
                        this.players[map_representation[x][y]["name"]].rotation = rotation;

                    }

                    // also add the ground mesh
                    var ground = new THREE.BoxGeometry(this.thickness, this.thickness / 4, this.thickness, 1, 1, 1);
                    new_map[x][y].mesh = new THREE.Mesh(ground, this.materials.ground);
                    new_map[x][y].mesh.visible = true;
                    new_map[x][y].mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                    this.scene.add(new_map[x][y].mesh);

                } else if (map_representation[x][y]["id"] === "SEEKER") {
                    // Add seeker
                    let rotation;
                    if (this.players[map_representation[x][y]["name"]] === undefined) {
                        // init seeker
                        const mtlLoader = new MTLLoader();
                        let map = new_map[x][y];
                        let map_representation_x_y = map_representation[x][y];
                        let position = [];
                        position["x"] = x * this.thickness - ((this.width * this.thickness) / 2);
                        position["y"] = this.thickness / 2;
                        position["z"] = y * this.thickness - ((this.height * this.thickness) / 2);
                        // load seeker
                        new Promise((resolve, reject) => {
                            mtlLoader.load('data/modeles/Coyote/wile.mtl', (materials) => {
                                materials.preload();
                                const objLoader = new OBJLoader();
                                objLoader.setMaterials(materials);
                                objLoader.load('data/modeles/Coyote/wile.obj', (objet) => {
                                    const boite = new THREE.Box3();
                                    boite.setFromObject(objet);
                                    let scale = this.thickness * 2 / boite.max.y;
                                    objet.scale.x = scale;
                                    objet.scale.y = scale;
                                    objet.scale.z = scale;
                                    resolve(objet)
                                })
                            })
                        })
                            .then(obj => {
                                    //add animation seeker finds hider
                                    let group = new THREE.Group();
                                    var wall_geometry = new THREE.BoxGeometry(1, 2.5, 1, 1, 1, 1);
                                    const diffus = new THREE.MeshBasicMaterial({
                                        color: 0xff0000, side: THREE.DoubleSide
                                    });
                                    var block = new THREE.Mesh(wall_geometry, diffus);
                                    block.visible = true;
                                    block.position.y = 3;
                                    group.add(block)
                                    var wall_geometry = new THREE.BoxGeometry(1, 1, 1, 1, 1, 1);
                                    var block = new THREE.Mesh(wall_geometry, diffus);
                                    block.visible = true;
                                    group.add(block);
                                    group.position.y = 15;
                                    obj.add(group);
                                    group.visible = false;

                                    map.player = obj;
                                    map.player.position.set(position["x"], position["y"], position["z"]);
                                    // add orientation
                                    if (map_representation_x_y["orientation"] === "Top") {
                                        map.player.rotateY(-Math.PI / 2);
                                        rotation = Math.PI / 2;

                                    } else if (map_representation_x_y["orientation"] === "Bottom") {
                                        map.player.rotateY(Math.PI / 2);
                                        rotation = -Math.PI / 2;

                                    } else if (map_representation_x_y["orientation"] === "Left") {
                                        map.player.rotateY(Math.PI);
                                        rotation = -Math.PI;
                                    } else {
                                        map.player.rotateY(0);
                                        rotation = 0;

                                    }
                                    this.players[map_representation_x_y["name"]] = {
                                        player: map.player,
                                        rotation: rotation,
                                    };
                                    this.scene.add(map.player);
                                }
                            )
                    } else {
                        // change of position
                        new_map[x][y].player = this.players[map_representation[x][y]["name"]].player;
                        new_map[x][y].player.visible = true;
                        var tween = new TWEEN.Tween(new_map[x][y].player.position).to({
                            x: x * this.thickness - ((this.width * this.thickness) / 2),
                            y: this.thickness / 2,
                            z: y * this.thickness - ((this.height * this.thickness) / 2)
                        }, 1000);
                        tween.start();

                        //add orientation
                        new_map[x][y].player.rotateY(this.players[map_representation[x][y]["name"]].rotation);
                        if (map_representation[x][y]["orientation"] === "Top") {
                            new_map[x][y].player.rotateY(-Math.PI / 2);
                            rotation = Math.PI / 2;

                        } else if (map_representation[x][y]["orientation"] === "Bottom") {
                            new_map[x][y].player.rotateY(Math.PI / 2);
                            rotation = -Math.PI / 2;

                        } else if (map_representation[x][y]["orientation"] === "Left") {
                            new_map[x][y].player.rotateY(Math.PI);
                            rotation = -Math.PI;
                        } else {
                            new_map[x][y].player.rotateY(0);
                            rotation = 0;

                        }
                        this.players[map_representation[x][y]["name"]].player = new_map[x][y].player;
                        this.players[map_representation[x][y]["name"]].rotation = rotation;

                    }

                    // also add the ground mesh
                    var ground = new THREE.BoxGeometry(this.thickness, this.thickness / 4, this.thickness, 1, 1, 1);
                    new_map[x][y].mesh = new THREE.Mesh(ground, this.materials.ground);
                    new_map[x][y].mesh.visible = true;
                    new_map[x][y].mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                    this.scene.add(new_map[x][y].mesh);

                } else if (map_representation[x][y]["id"] === "WOOD") {
                    //console.log(map_representation["resistance"])
                    // generate the mesh with wood
                    var wall_geometry = new THREE.BoxGeometry(this.thickness, this.thickness * 2 * (map_representation[x][y]["resistance"] / 100), this.thickness, 1, 1, 1);
                    new_map[x][y].mesh = new THREE.Mesh(wall_geometry, this.materials.wood);
                    new_map[x][y].mesh.visible = true;
                    new_map[x][y].mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                    this.scene.add(new_map[x][y].mesh);
                } else if (map_representation[x][y]["id"] === "STONE") {
                    // generate the mesh with stone
                    var wall_geometry = new THREE.BoxGeometry(this.thickness, this.thickness * 2 * (map_representation[x][y]["resistance"] / 100), this.thickness, 1, 1, 1);
                    new_map[x][y].mesh = new THREE.Mesh(wall_geometry, this.materials.stone);
                    new_map[x][y].mesh.visible = true;
                    new_map[x][y].mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                    this.scene.add(new_map[x][y].mesh);
                } else {
                    // by default other border
                    // generate the mesh with metal (obidian)
                    var wall_geometry = new THREE.BoxGeometry(this.thickness, this.thickness * 2 * (map_representation[x][y]["resistance"] / 100), this.thickness, 1, 1, 1);
                    new_map[x][y].mesh = new THREE.Mesh(wall_geometry, this.materials.metal);
                    new_map[x][y].mesh.visible = true;
                    new_map[x][y].mesh.position.set(x * this.thickness - ((this.width * this.thickness) / 2), 0, y * this.thickness - ((this.height * this.thickness) / 2));
                    this.scene.add(new_map[x][y].mesh);
                }
            }
        }

        this.map_representation = map_representation;
        this.map = new_map;
    };

    /**
     * Inits the scene
     */
    initScene() {
        // Scene
        this.scene = new THREE.Scene();
        this.scene.background = new THREE.Color(0xffffff);

        // Materials
        const loader = new THREE.TextureLoader();

        const ground_material = new THREE.MeshBasicMaterial({
            map: loader.load('data/textures/grass.png'),
        })

        const wall_material = new THREE.MeshBasicMaterial({
            map: loader.load('data/textures/border.png'), wireframe: false
        })
        const wood_material = new THREE.MeshBasicMaterial({
            map: loader.load('data/textures/wood.png'), wireframe: false
        })
        const stone_material = new THREE.MeshBasicMaterial({
            map: loader.load('data/textures/stone.jpeg'), wireframe: false
        })
        const metal_material = new THREE.MeshBasicMaterial({
            map: loader.load('data/textures/obsidian.jpg'), wireframe: false
        })

        this.materials = {
            grey: wall_material,
            wood: wood_material,
            stone: stone_material,
            metal: metal_material,
            ground: ground_material,
            hider: new THREE.MeshLambertMaterial({color: 0x0066ff}),
            seeker: new THREE.MeshLambertMaterial({color: 0x990000}),
            orange: new THREE.MeshLambertMaterial({color: 0xbe842b}),
        };

        // Camera
        this.camera = new THREE.PerspectiveCamera(45, 1, 1, 2000);
        this.camera.position.x = 1000;
        this.camera.position.y = 800;
        this.camera.position.z = -300;

        this.camera.clicked = false;

        // Lights
        this.scene.add(new THREE.AmbientLight(0xc9c9c9));
        var directional = new THREE.DirectionalLight(0xc9c9c9, 0.5);
        directional.position.set(0, 0.5, 1);
        this.scene.add(directional);

        // this.camera.lookAt(this.scene.position);
        // Renderer
        this.renderer = typeof WebGLRenderingContext != 'undefined' && window.WebGLRenderingContext ? new THREE.WebGLRenderer({antialias: true}) : new THREE.CanvasRenderer({});
        this.wrapper.appendChild(this.renderer.domElement);
    };

    /**
     * Render loop
     * Sets the camera position and renders the scene
     */
    render() {
        requestAnimationFrame(this.render.bind(this));

        TWEEN.update();
        this.camera.lookAt(new THREE.Vector3(0, -150, 0));
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.render(this.scene, this.camera);
    };

    /**
     * Sets the scene dimensions on window resize
     */
    onWindowResize() {
        // Check if there is a better way to adjust width and height
        var width = (this.window.innerWidth / 12) * 8.5 || this.window.document.body.clientWidth;
        var height = this.window.innerHeight - 120 || this.window.document.body.clientHeight;
        this.renderer.setSize(width, height);
        this.camera.aspect = width / height;
        this.camera.updateProjectionMatrix();
    };
}

export default ThreeMaze;
