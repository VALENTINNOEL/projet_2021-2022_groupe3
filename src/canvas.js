import ThreeMaze from './maze.js'
import Alpine from 'alpinejs'
import {SERVER_URL} from './params.js'
import {round, sqrt} from "three/examples/jsm/nodes/shadernode/ShaderNodeBaseElements";
import Chart from 'chart.js/auto'
import data from "alpinejs";

var maze = undefined;
const wrapper = document.querySelector('.three');

const ctx = document.getElementById('myChart').getContext('2d');
var myChart = undefined;

const load = () => {

    try{
        myChart.destroy();
    } catch (e) {

    }

    myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: [],
            datasets: [{
                label: 'Average',
                backgroundColor: 'rgb(46, 0, 108)',
                borderColor: 'rgb(46, 0, 108)',
                data: [],
            }, {
                label: 'S2 vs H1',
                backgroundColor: 'rgb(255, 0, 0)',
                borderColor: 'rgb(255, 0, 0)',
                data: [],
            }, {
                label: 'S1 vs H1',
                backgroundColor: 'rgb(155, 25, 25)',
                borderColor: 'rgb(155, 25, 25)',
                data: [],
            }, {
                label: 'S2 vs H2',
                backgroundColor: 'rgb(30, 144, 255)',
                borderColor: 'rgb(30, 144, 255)',
            }, {
                label: 'S1 vs H2',
                backgroundColor: 'rgb(39, 94, 162)',
                borderColor: 'rgb(39, 94, 162)',
                data: [],
            }],
        },
        options: {
            scales: {x: {title: {display: true, text: 'Rounds'}}, y: {title: {display: true, text: 'Distance'}}}
        }
    });

    let id = Alpine.store('gameId');
    fetch(`${SERVER_URL}/game?id=${id}`)
        .then(res => {

            // check for error response
            if (!res.ok) {
                // get error message from body or default to response status
                const error = response.status;
                return Promise.reject(error);
            }

            // remove info message (if exists)
            Alpine.store('gameInfo', "");

            return res.json();
        })
        .then(data => {
            console.log(data);
            // initialize game
            wrapper.innerHTML = ""
            Alpine.store('gameEnd', false);
            Alpine.store('gameRound', data["round"]);
            Alpine.store('gameMaxRounds', data["maxRounds"]);
            Alpine.store('gameHider', data["hider"]);
            Alpine.store('gameSeeker', data["seeker"]);
            Alpine.store('boardStats', data["cells"]);

            for (let i = 0; i < data['players'].length; i++) {
                data['players'][i]['steps'] = 0;
                data['players'][i]['id'] = i + 1;
                if (data['players'][i].roleStr === "SEEKER") {
                    data['players'][i]['hunted'] = 0;
                    data['players'][i]['brokenBlocs'] = 0;
                }
                if (data['players'][i].roleStr === "HIDER") data['players'][i]['carriedBlocs'] = 0;
            }

            Alpine.store('players', data['players']);

            getAverageDistance(data);

            let width = data['width']
            let height = data['height']

            let percentProgress = parseInt(parseFloat(data['round']) / parseFloat(data['maxRounds']) * 100.);

            Alpine.store('gameProgress', percentProgress);

            maze = new ThreeMaze(window, wrapper, width, height);

            // Inits
            maze.initScene();
            maze.onWindowResize();
            maze.render();

            var map = [];
            let counter = 0;
            for (var x = 1; x <= width; x += 1) {
                map[x] = [];

                for (var y = 1; y <= height; y += 1) {
                    map[x][y] = [];
                    if ('block' in data['cells'][counter]) {

                        if (data['cells'][counter]["block"]["kindStr"] !== "MATERIAL") {
                            map[x][y]["id"] = data['cells'][counter]["block"]["kindStr"];
                        } else {
                            map[x][y]["id"] = data['cells'][counter]["block"]["materialStr"];
                            map[x][y]["resistance"] = data['cells'][counter]["block"]["resistance"];
                        }

                    }
                    if ('player' in data['cells'][counter]) {
                        map[x][y]["id"] = data['cells'][counter]["player"]["roleStr"];
                        map[x][y]["orientation"] = data['cells'][counter]["player"]["orientationStr"];
                        map[x][y]["name"] = data['cells'][counter]["player"]["name"];
                        map[x][y]["block"] = [];
                        map[x][y]["block"]["materialStr"] = undefined;
                        map[x][y]["block"]["visible"] = false;

                    }
                    counter++;
                }
            }
            map['captured'] = [];
            map["observationSeeker"] = undefined;

            // load maze map
            maze.onGenerateMaze(map);

            window.addEventListener('resize', maze.onWindowResize.bind(maze));

            //Map
            removeMiniMap()
            miniMap(data)
        }).catch(e => {
        console.log(e)
        Alpine.store('gameInfo', "Game not found...");
        Alpine.store('gameError', true);
    });
}

const updateLoop = () => {

    let id = Alpine.store('gameId');

    let oldPlayerStats = Alpine.store('players');
    let oldBoardStats = Alpine.store('boardStats');
    let oldRound = Alpine.store('gameRound')

    fetch(`${SERVER_URL}/game/step`, {
        method: 'POST', body: JSON.stringify({"id": id})
    })
        .then(res => {

            // check for error response
            if (!res.ok) {
                // get error message from body or default to response status
                const error = res.status;
                return Promise.reject(error);
            }

            // remove error message (if exists)
            Alpine.store('gameInfo', "");

            return res.json();
        })
        .then(data => {

            //Map
            removeMiniMap()
            miniMap(data)

            // no server error currently
            Alpine.store('gameError', false);
            Alpine.store('gameRound', data["round"]);
            Alpine.store('gameMaxRounds', data["maxRounds"]);
            Alpine.store('gameHider', data["hider"]);
            Alpine.store('gameSeeker', data["seeker"]);
            Alpine.store('boardStats', data["cells"]);

            for (let i = 0; i < data['players'].length; i++) {
                let locationX = data['players'][i].location.x;
                let locationY = data['players'][i].location.y;

                data['players'][i].id = oldPlayerStats[i].id;
                data['players'][i].steps = oldPlayerStats[i].steps;

                if (data['players'][i].roleStr === "HIDER") {
                    data['players'][i].carriedBlocs = oldPlayerStats[i].carriedBlocs;
                }

                if (data['players'][i].roleStr === "SEEKER") {
                    data['players'][i].brokenBlocs = oldPlayerStats[i].brokenBlocs;
                    data['players'][i].hunted = oldPlayerStats[i].hunted;
                }

                //Steps
                if (locationX !== oldPlayerStats[i].location.x || locationY !== oldPlayerStats[i].location.y) {
                    data['players'][i].steps += 1;
                }

                //Broken blocks
                if (data['currentAction'].interaction === 1 && data['currentPlayer'].name === data['players'][i].name) {
                    for (let c = 0; c < data['cells'].length; c++) {
                        if (data['cells'][c].block.kindStr !== oldBoardStats[c].block.kindStr) {
                            data['players'][i].brokenBlocs += 1;
                        }
                    }
                }

                //Moved blocks
                if (data['currentAction'].interaction === 2 && data['currentPlayer'].name === data['players'][i].name) {
                    data['players'][i].carriedBlocs += 1;
                }

                //Hiders caught
                if (data['players'][i].captured !== oldPlayerStats[i].captured) {
                    for (let j = 0; j < data['players'].length; j++) {
                        if (data['players'][j].roleStr === "SEEKER") {
                            if (data['players'][i].location.x === data['players'][j].location.x && data['players'][i].location.y === data['players'][j].location.y) {
                                data['players'][j].hunted += 1;
                            }
                        }
                    }
                }
            }

            //Average distance between Seekers and Hiders
            if (data.round !== oldRound) {
                getAverageDistance(data);
            }

            Alpine.store('players', data['players']);

            let width = data['width']
            let height = data['height']
            console.log(data['round'], 'of', data['maxRounds'], '=> end? : ', data['end']);
            let gameEnd = data['end'];

            Alpine.store('gameEnd', gameEnd);

            if (gameEnd) {
                // clearInterval(canvasInterval);
                let currentWinner = data['winnerStr'];
                Alpine.store('gameInfo', `Game ${Alpine.store("gameId")} is finished! Winner is: ${currentWinner}`)
                Alpine.store('gameWinner', currentWinner);
            }

            let percentProgress = parseInt(parseFloat(data['round']) / parseFloat(data['maxRounds']) * 100.);

            Alpine.store('gameProgress', percentProgress);

            var map = [];
            let counter = 0;

            for (var x = 1; x <= width; x += 1) {
                map[x] = [];

                for (var y = 1; y <= height; y += 1) {
                    map[x][y] = [];
                    if ('block' in data['cells'][counter]) {

                        if (data['cells'][counter]["block"]["kindStr"] !== "MATERIAL") {
                            map[x][y]["id"] = data['cells'][counter]["block"]["kindStr"];
                        } else {
                            map[x][y]["id"] = data['cells'][counter]["block"]["materialStr"];
                            map[x][y]["resistance"] = data['cells'][counter]["block"]["resistance"];
                        }

                    }
                    if ('player' in data['cells'][counter]) {
                        map[x][y]["id"] = data['cells'][counter]["player"]["roleStr"];
                        map[x][y]["orientation"] = data['cells'][counter]["player"]["orientationStr"];
                        map[x][y]["name"] = data['cells'][counter]["player"]["name"];
                        map[x][y]["block"] = [];
                        if (data['cells'][counter]["player"]["block"] !== undefined) {
                            map[x][y]["block"]["visible"] = true;
                            map[x][y]["block"]["materialStr"] = data['cells'][counter]["player"]["block"]["materialStr"];
                        } else {
                            map[x][y]["block"]["visible"] = false;
                            map[x][y]["block"]["materialStr"] = undefined;
                        }
                    }
                    counter++;
                }
            }
            map['captured'] = [];
            for (let i = 0; i < data['players'].length; i++) {
                if (data['players'][i].captured === true) map['captured'].push(data['players'][i].name);
            }

            map["observationSeeker"] = undefined;
            if (data["currentPlayer"]["roleStr"] === "SEEKER") {
                map["observationSeeker"] = [];
                map["observationSeeker"]["name"] = data["currentPlayer"]["name"];
                map["observationSeeker"]["visible"] = false;
                for (let i = 0; i < data['currentObservation']["cells"].length; i++) {
                    if ("player" in data['currentObservation']["cells"][i]) {
                        if (data['currentObservation']["cells"][i]["player"]["roleStr"] === 'HIDER') {
                            map["observationSeeker"]["visible"] = true;
                        }
                    }

                }
            }

            // reload maze map
            if (maze !== undefined) {
                maze.onGenerateMaze(map);
            }
        })
        .catch((e) => {
            console.log(e)

            Alpine.store('gameInfo', "Server cannot be reached");
            Alpine.store('gameError', true);
        });
}

function getAverageDistance(data) {
    let averageDistance = 0;
    let dist1, dist2, dist3, dist4 = 0.0;
    let cpt = 0;
    myChart.data.labels.push(data.round);

    for (let i = 0; i < data['players'].length; i++) {
        if (data['players'][i].name === "H1" && data['players'][i].captured !== true) {
            for (let j = 0; j < data['players'].length; j++) {
                if (data['players'][j].name === "S1") {
                    dist1 = Math.sqrt((Math.pow(data['players'][i].location.x - data['players'][j].location.x, 2))
                        + (Math.pow(data['players'][i].location.y - data['players'][j].location.y, 2)));
                    averageDistance += dist1;
                    cpt++;
                }
                if (data['players'][j].name === "S2") {
                    dist2 = Math.sqrt((Math.pow(data['players'][i].location.x - data['players'][j].location.x, 2))
                        + (Math.pow(data['players'][i].location.y - data['players'][j].location.y, 2)));
                    averageDistance += dist2;
                    cpt++;
                }
            }
        }
        if (data['players'][i].name === "H2" && data['players'][i].captured !== true) {
            for (let j = 0; j < data['players'].length; j++) {
                if (data['players'][j].name === "S1") {
                    dist3 = Math.sqrt((Math.pow(data['players'][i].location.x - data['players'][j].location.x, 2))
                        + (Math.pow(data['players'][i].location.y - data['players'][j].location.y, 2)));
                    averageDistance += dist3;
                    cpt++;
                }
                if (data['players'][j].name === "S2") {
                    dist4 = Math.sqrt((Math.pow(data['players'][i].location.x - data['players'][j].location.x, 2))
                        + (Math.pow(data['players'][i].location.y - data['players'][j].location.y, 2)));
                    averageDistance += dist4;
                    cpt++
                }
            }
        }
    }
    console.log(averageDistance, dist1, dist2, dist3, dist4);
    myChart.data.datasets[0].data.push(averageDistance/cpt);
    myChart.data.datasets[1].data.push(dist2);
    myChart.data.datasets[2].data.push(dist1);
    myChart.data.datasets[3].data.push(dist4);
    myChart.data.datasets[4].data.push(dist3);
    myChart.update();
}

function miniMap(data){
    let mapcontain = document.getElementById('map-container')
    let table = document.createElement('table')
    /*table.classList.add('col-md-3')*/
    table.id = "map"
    mapcontain.appendChild(table)

    for(let l = 0; l < data['height']; l++){
        let line = document.createElement('tr')
        table.appendChild(line)
        for(let c = 0; c < data['width']; c++){
            let row = document.createElement('td')
            line.appendChild(row)
            for(let p = 0; p < data['players'].length; p++){
                if(data['players'][p].location.x + data['players'][p].location.y * data['width'] === c + l * data['width']){
                    if(data['players'][p].roleStr === "HIDER"){
                        if(!data['players'][p].captured) row.classList.add("hider");
                    }
                    else row.classList.add("seeker");
                }else{
                    if(data['cells'][c + l * data['width']].block.kindStr === "BORDER") row.classList.add('border');
                    else if(data['cells'][c + l * data['width']].block.kindStr === "MATERIAL"){
                        if(data['cells'][c + l * data['width']].block.materialStr === "WOOD") row.classList.add('wood');
                        else if(data['cells'][c + l * data['width']].block.materialStr === "STONE") row.classList.add('stone');
                        else if(data['cells'][c + l * data['width']].block.materialStr === "METAL") row.classList.add('obsidian');
                    }
                    else if(data['cells'][c + l * data['width']].block.kindStr === "GROUND") row.classList.add('ground');
                }
            }
        }
    }
}

function removeMiniMap(){
    try{
        let table = document.getElementById('map')
        table.remove()
    }catch (e){}
}

export {load, updateLoop};
